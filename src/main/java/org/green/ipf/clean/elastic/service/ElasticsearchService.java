package org.green.ipf.clean.elastic.service;

import org.green.ipf.clean.elastic.response.model.ResponseIndice;
import org.green.ipf.clean.elastic.response.model.ResponseTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class ElasticsearchService {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RestTemplate restTemplate;

    public void deleteIndices(String ip, String port) {
        StringBuilder sb = new StringBuilder("indices: ").append("\n");

        ResponseEntity<ResponseIndice[]> responseIndices =
                restTemplate.getForEntity("http://"+ ip + ":" + port + "/_cat/indices?format=json", ResponseIndice[].class);

        for (ResponseIndice responseIndice: responseIndices.getBody()) {
            sb.append(responseIndice.getIndex()).append("\n");
            String deleteUrl = "http://"+ ip + ":" + port + "/" + responseIndice.getIndex();

            try {
                restTemplate.delete(deleteUrl);
                LOGGER.info("DELETE template {}", deleteUrl);
            } catch (HttpClientErrorException e){
                LOGGER.error("{}, {}", deleteUrl,e.getMessage());
            }
        }

        LOGGER.info("{}", sb);
    }

    public void deleteTemplates(String ip, String port) {
        StringBuilder sb = new StringBuilder("templates: ").append("\n");

        // Fetch Templates
        ResponseEntity<ResponseTemplate[]> entity =
                restTemplate.getForEntity("http://"+ ip + ":" + port + "/_cat/templates?format=json", ResponseTemplate[].class);

        // Delete templates
        for (ResponseTemplate responseTemplate: entity.getBody()) {
            String deleteUrl = "http://"+ ip + ":" + port + "/_template/"+responseTemplate.getName();

            try {
                sb.append(responseTemplate.getName()).append("\n");
                restTemplate.delete(deleteUrl);
                LOGGER.info("DELETE {}", deleteUrl);
            } catch (HttpClientErrorException e){
                LOGGER.error("{}, {}", deleteUrl,e.getMessage());
            }
        }

        LOGGER.info("{}", sb);
    }
}
