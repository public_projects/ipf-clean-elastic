package org.green.ipf.clean.elastic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class IpfCleanElasticApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpfCleanElasticApplication.class, args);
	}

}
