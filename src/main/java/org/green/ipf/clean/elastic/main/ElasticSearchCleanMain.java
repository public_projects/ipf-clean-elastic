package org.green.ipf.clean.elastic.main;

import org.green.ipf.clean.elastic.service.ElasticsearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ElasticSearchCleanMain implements CommandLineRunner {
    @Autowired
    private ElasticsearchService elasticsearchService;

    @Override
    public void run(String... args) throws Exception {
        String ip = "localhost";
        String port = "6200";
        elasticsearchService.deleteIndices(ip, port);
        elasticsearchService.deleteTemplates(ip, port);
    }
}
