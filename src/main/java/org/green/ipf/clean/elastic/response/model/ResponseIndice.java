package org.green.ipf.clean.elastic.response.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseIndice {
    @JsonSetter("docs.count")
    private String docCount;
    @JsonSetter("docs.deleted")
    private String docDeleted;
    @JsonSetter("store.size")
    private String storeSize;
    private String status;
    private String index;

    public String getDocCount() {
        return docCount;
    }

    public void setDocCount(String docCount) {
        this.docCount = docCount;
    }

    public String getDocDeleted() {
        return docDeleted;
    }

    public void setDocDeleted(String docDeleted) {
        this.docDeleted = docDeleted;
    }

    public String getStoreSize() {
        return storeSize;
    }

    public void setStoreSize(String storeSize) {
        this.storeSize = storeSize;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResponseIndice that = (ResponseIndice) o;
        return Objects.equals(storeSize, that.storeSize) && Objects.equals(docCount, that.docCount) && Objects.equals(docDeleted, that.docDeleted) &&
               Objects.equals(status, that.status) && Objects.equals(index, that.index);
    }

    @Override
    public int hashCode() {
        return Objects.hash(docCount, docDeleted, storeSize, status, index);
    }
}
