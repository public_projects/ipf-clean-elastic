package org.green.ipf.clean.elastic.response.model;

import java.util.Objects;

public class ResponseTemplate {
    private String name;
    private String index_patterns;
    private String order;
    private String version;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndex_patterns() {
        return index_patterns;
    }

    public void setIndex_patterns(String index_patterns) {
        this.index_patterns = index_patterns;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResponseTemplate that = (ResponseTemplate) o;
        return Objects.equals(name, that.name) && Objects.equals(index_patterns, that.index_patterns) && Objects.equals(order,
                                                                                                                        that.order) &&
               Objects.equals(version, that.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, index_patterns, order, version);
    }
}
